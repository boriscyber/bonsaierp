require 'spec_helper'

describe Loans::ReceivePaymentForm do
  #it { should validate_presence_of(:reference) }

  let(:loan_attr) do
    today = Date.today
    {
      date: today, due_date: today + 10.days, total: 100,
      reference: 'Receipt 00232', contact_id: 1
    }
  end


  it "validate" do
    lp = Loans::PaymentForm.new
    lp.stub(loan: Loan.new(total: 100))
    lp.valid?
  end

  context 'payment' do
    let(:cash) { create :cash, currency: 'BOB', amount: 0 }
    let(:contact) { build :contact, id: 1 }


    let(:attributes) do
      {
        account_to_id: cash.id, date: Date.today, reference: 'Pay 23233',
        date: Date.today, amount: 50, exchange_rate: 1
      }
    end

    before(:each) {
      UserSession.user = build :user, id: 1
      OrganisationSession.organisation = build :organisation, currency: 'BOB'
      Loans::Receive.any_instance.stub(contact: contact)
    }

    it "sets ledger status" do
      lp = Loans::ReceivePaymentForm.new(verification: '1', account_to_id: 2)
      lp.stub(loan: Loans::Receive.new(id: 10))
      lp.ledger.should be_is_pendent

      lp.int_ledger.should be_is_pendent
    end

    it "pays Loan" do
      lf = Loans::ReceiveForm.new(loan_attr.merge(account_to_id: cash.id))

      lf.create.should be_true
      lf.loan.should be_persisted
      lf.loan.should be_is_a(Loans::Receive)
      lf.loan.amount.should == 100
      lf.loan.total.should == 100
      lf.loan.ledger_in.should be_is_a(AccountLedger)
      lf.loan.ledger_in.should be_is_lrcre

      lf.ledger.contact_id.should_not be_blank
      lf.ledger.contact_id.should eq(lf.loan.contact_id)

      cash.reload.amount.should == 100

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id))

      lp.create_payment.should be_true
      lp.ledger.amount.should == -50
      lp.ledger.currency.should eq('BOB')
      lp.ledger.should be_is_approved

      lp.ledger.contact_id.should_not be_blank
      lp.ledger.contact_id.should eq(lf.loan.contact_id)

      loan = Loans::Receive.find(lf.loan.id)
      loan.amount.should == 50

      c = Cash.find(cash.id)
      c.amount.should == 50

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 60))
      lp.create_payment.should be_false
      lp.errors[:amount].should eq([I18n.t('errors.messages.less_than_or_equal_to', count: 50.0)])
      # Pay with other currency
      bank = create :bank, currency: 'USD', amount: 0

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 25, account_to_id: bank.id, exchange_rate: 2))

      lp.create_payment.should be_true
      loan = Loans::Receive.find(loan.id)

      loan.amount.should == 0
      loan.should be_is_paid
    end

    # Pay with income
    it "pay with income" do
      lf = Loans::ReceiveForm.new(loan_attr.merge(account_to_id: cash.id, total: 200))

      lf.create.should be_true
      lf.loan.amount.should == 200
      lf.loan.should be_is_approved

      today = Date.today
      income = Income.new(total: 100, balance: 100, state: 'approved', currency: 'BOB', id: 100, contact_id: 1,
                         date: today, due_date: today, ref_number: 'I-13-0001')
      Income.any_instance.stub(contact: build(:contact, id: 1))
      income.save.should be_true

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 200, account_to_id: income.id))

      # Validate for income amount
      lp.create_payment.should be_false
      lp.errors[:amount].should eq(['La cantidad es mayor que el saldo del Ingreso'])


      lp.amount = 100
      lp.create_payment.should be_true

      inc = Income.find(income.id)
      inc.amount.should == 0
      inc.should be_is_paid

      l = Loans::Receive.find(lf.loan.id)
      l.amount.should == 100
      l.should be_is_approved

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 200, account_to_id: cash.id))
      lp.create_payment.should be_false

      lp.errors[:amount].should_not be_blank
    end


    it "pays interest" do
      lf = Loans::ReceiveForm.new(loan_attr.merge(account_to_id: cash.id))

      lf.create.should be_true
      lf.loan.should be_persisted
      lf.loan.ledger_in.should be_is_a(AccountLedger)
      cash.reload.amount.should == 100

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id))

      lp.create_interest.should be_true
      lp.int_ledger.amount.should == -50
      lp.int_ledger.should be_is_lrint
      lp.int_ledger.should be_persisted

      lp.int_ledger.contact_id.should_not be_blank
      lp.int_ledger.contact_id.should eq(lf.loan.contact_id)

      loan = Loans::Receive.find(lf.loan.id)
      loan.interests.should == 50

      c = Cash.find(cash.id)
      c.amount.should == 50
    end

    # Pay with INTERESTS with income
    it "pay INTERESTS with income" do
      lf = Loans::ReceiveForm.new(loan_attr.merge(account_to_id: cash.id, total: 200))

      lf.create.should be_true
      lf.loan.amount.should == 200
      lf.loan.should be_is_approved

      today = Date.today
      income = Income.new(total: 100, balance: 100, state: 'approved', currency: 'BOB', id: 100, contact_id: 1,
                         date: today, due_date: today, ref_number: 'I-13-001')
      Income.any_instance.stub(contact: build(:contact, id: 1))
      income.save.should be_true

      lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 200, account_to_id: income.id))

      lp.create_interest.should be_false
      lp.amount = 100

      lp.create_interest.should be_true
      lp.int_ledger.should be_persisted
      lp.int_ledger.amount.should == -100
      lp.int_ledger.should be_is_lrint

      inc = Income.find(income.id)
      inc.amount.should == 0
      inc.should be_is_paid

      # No changes to the amount
      l = Loans::Receive.find(lf.loan.id)
      l.amount.should == 200
      l.should be_is_approved
    end

    context 'other currencies' do
      let(:cash2) { create :cash, currency: 'USD', amount: 0 }

      it 'USD and BOB' do
        lf = Loans::ReceiveForm.new(loan_attr.merge(account_to_id: cash2.id))

        lf.create.should be_true
        lf.loan.should be_persisted
        lf.loan.should be_is_a(Loans::Receive)
        lf.loan.amount.should == 100
        lf.loan.total.should == 100
        lf.loan.currency.should eq('USD')
        lf.loan.ledger_in.should be_is_a(AccountLedger)
        lf.loan.ledger_in.should be_is_lrcre
        cash2.reload.amount.should == 100

        lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, exchange_rate: 5))

        lp.create_payment.should be_true
        lp.ledger.amount.should == -50
        lp.ledger.currency.should eq('BOB')

        loan = Loans::Receive.find(lf.loan.id)
        loan.amount.should == 90

        c = Cash.find(cash.id)
        c.amount.should == -50

        lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 95, account_to_id: cash2.id))
        lp.create_payment.should be_false
        lp.errors[:amount].should eq([I18n.t('errors.messages.less_than_or_equal_to', count: 90.0)])

        # Pay in USD

        lp = Loans::ReceivePaymentForm.new(attributes.merge(account_id: lf.loan.id, amount: 90, account_to_id: cash2.id))

        lp.create_payment.should be_true
        loan = Loans::Receive.find(loan.id)

        loan.amount.should == 0
        loan.should be_is_paid
      end
    end

  end

end
